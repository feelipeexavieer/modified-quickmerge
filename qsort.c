#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <dirent.h>
#include <string.h>

#include <time.h>

//ESCREVE VETOR RECEBIDO
void imprimeVetor(int *vetor, int tamanho)
{
    int i;

    printf("[");
    for(i = 0; i < tamanho; i++)
        printf("%d, ", vetor[i]);
    printf("]\n");
}

//PROCURA O TAMANHO DO ARQUIVO QUE PRECISA SER ORDENADO
int getTamanhoArquivo(char *sArquivo)
{
    FILE* fp = fopen(sArquivo, "r"); 
  
    if (fp == NULL) 
        return -1; 
  
    fseek(fp, 0L, SEEK_END); 
  
    long int tam = ftell(fp); 
 
    fclose(fp); 
  
    return tam/4; 
}

void lerArquivo(char *sArquivo, int *vetor)
{
    FILE *file;
    int i = 0;

    if( (file = fopen(sArquivo, "rb")) == NULL)
    {
        printf("ERRO: Arquivo não pode ser aberto\n");
        return;
    }

    while(!feof(file))
    {
        if( fread(& (* (vetor + i ) ), sizeof(int), 1, file) != 1 )
        {
            if(feof(file)) break;
            printf("Erro de leitura do arquivo %s", sArquivo);
        }

        i++;
    }

    fclose(file);
}

//FUNCAO QUE COMPARA DOIS ELEMENTOS
int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

int testOrdenacao(int *vetor, int tamanho)
{
    int i;

    tamanho--;

    for(i = 0; i < tamanho; i++)
    {
        if(vetor[i] > vetor[i+1])
            return -1;
    }

    return +1;
}

int main(int argc, char *argv[])
{
    char *sArquivo; 
    int *vetor = NULL;  
    int qtdeElementos; 

    if(argc != 2)
    {
        printf("ERRO: Informe o nome do arquivo.\n");
        return -1;
    }
    else
        sArquivo = argv[1];                                                     

    qtdeElementos = getTamanhoArquivo(sArquivo);

    vetor = (int *) malloc(qtdeElementos * sizeof(int));

    printf("Lendo elementos \n");

    lerArquivo(sArquivo, vetor);

    printf("Elementos lidos!\n");

    clock_t t = clock(); 

    qsort(vetor, qtdeElementos, sizeof(int), cmpfunc);

    t = clock() - t; 
    
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // em segundos 

    if(testOrdenacao(vetor, qtdeElementos) > 0)
        printf("%d Elementos ordenados corretamente.\n", qtdeElementos);
    else
        printf("ERRO: Falha de ordenacao.\n");

    printf("tempo de ordenacao: %f segundos\n", time_taken);

    //imprimeVetor(vetor, qtdeElementos);

    free(vetor);

    return +1;
}
