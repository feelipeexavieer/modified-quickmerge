#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include <dirent.h>
#include <string.h>

// PROCESSOS/RANKS
struct rank 
{
	int my_rank;                                     // IDENTIFICADOR DO PROCESSO
	int comm_sz;									 // QUANTIDADE DE PROCESSOS
	int parceiro_rank;								 // IDENTIFICADOR DO PROCESSO PARCEIRO
	int *list;										 // LISTA LOCAL DE ELEMENTOS
	int *lowList;									 // LISTA LOCAL DOS MAIORES ELEMENTOS
	int *highList;									 // LISTA LOCAL DOS MENORES ELEMENTOS
	int lengthList, lengthHighList, lengthLowList;   // QUANTIDADE DE ELEMENTOS DE CADA LISTA
	int pivo;										 // PIVÔ PARTICIONADOR
	int qtdeElementos;								 // QUANTIDADE TOTAL DE ELEMENTOS
	int elementosPorProcesso;						 // QUANTIDADE DE ELEMENTOS POR PROCESSO

	int root_rank;									 // IDENTIFICADOR DO PROCESSO RAIZ
	char *sArquivo;									 // NOME DO ARQUIVO A SER ORDENADO

	MPI_Comm Comm;									 // COMUNICADOR DO MPI

	int fatorMemoria;                                // FATOR ALOCAÇÃO DE MEMORIA
	int sizeList; 									 // MEMÓRIA ALOCADA PARA LISTA LOCAL DE ELEMENTOS
	int sizeHighList;								 // MEMÓRIA ALOCADA PARA A LISTA COM MAIORES ELEMENTOS
	int sizeLowList;								 // MEMÓRIA ALOCADA PARA A LISTA COM MENORES ELEMENTOS

	int *vetorTemp;                                  // VETOR TEMPORARIO
    int vetorTempLength;                             // TAMANHO DO VETOR TEMPORARIO
};

typedef struct rank Processo;

// ALOCA MEMÓRIA PARA AS LISTAS DE ELEMENTOS
int alocaMemoria(Processo *processo);
void deleteProcesso(Processo *processo);

//ORDENAÇÃO

void quickMerge(Processo *processo);
//SEPARA A LISTA ENTRE NUMEROS MAIORES (HIGHLIST) E MENORES (LOWLIST) QUE O PIVO
int partitionList(Processo *processo);
// MERGE AS DUAS LISTAS, REUNI AS DUAS LISTAS EM UMA LISTA ORDENADA USANDO QUICKSORT
int merge(Processo *processo);
//FUNCAO QUE COMPARA DOIS ELEMENTOS
int cmpfunc (const void * a, const void * b);
//ESCREVE VETOR RECEBIDO
void imprimeVetor(int *vetor, int tamanho);
//IMPLEMENTACAO DA FUNCAO POW COM UM CASTING PARA int 
int powCast(int b, int e);
// COPIA VALORES DO VETOR SOURCE PARA O VETOR DEST
int copiaVetor(int *dest, int *source, int tamanho);
//RETORNA O TAMANHO DA MENSAGEM ENVIADA AO PROCESSO
int getMensagemLength(Processo *processo);

//ARQUIVOS

void salvarArquivo(Processo *processo);
void lerArquivo(Processo *processo, int *vetor);
//RETORNA NOME DO ARQUIVO QUE PRECISA SER ORDENADO
char *getNomeArquivo();
//RETORNA O TAMANHO DO ARQUIVO QUE PRECISA SER ORDENADO
long int getTamanhoArquivo();
