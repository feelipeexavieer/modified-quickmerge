#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int powCast(int b, int e)
{
    if(e == 0)
        return 1;

    int i;
    int p = b;
    e--;

    for(i = 0; i < e; i++)
        p *= b;
    
    return p;
}

int main()
{
	int dim, i, j;

	printf("Informe a dimensao do hipercubo: ");
	scanf("%d", &dim);

	printf("\n\n");

	int com_sz = powCast(2, dim);
	int parceiro_rank, index;

	int k; 

	for(i = dim -1, k = 0; i >= 0; i--, k++)
    {	
    	printf("\n\n---------------- RODADA %d ----------------\n", i);

    	for(j = 0; j < com_sz; j++)
    	{
    		parceiro_rank = j ^ powCast(2,i); 
    		index = (j & (powCast(2,dim)-powCast(2,dim-k))) | powCast(2, dim-k-1);
    		//index = (j & (powCast(2,dim)-powCast(2,dim-i))) | powCast(2, dim-1);
    		//index = (j & (powCast(2,dim)-powCast(2,dim-1))) | powCast(2, dim-1);

    		printf("\n\nProcesso %d, Parceiro %d, Index %d\n", j, parceiro_rank, index);
    	}
    }
}